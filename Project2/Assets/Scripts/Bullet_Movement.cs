﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Movement : MonoBehaviour {

	public float moveSpeed;
	public float destroyTime;
	public float damagePoints;

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, destroyTime);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.forward * moveSpeed * Time.deltaTime);
	}

	public void OnTriggerEnter(Collider other){
		//Debug.Log ("Boom");
		if (other.CompareTag ("Enemy"))
        {
            if (other.gameObject.GetComponent<Enemy_Controller>() != null)
            {
                other.gameObject.GetComponent<Enemy_Controller>().enemyHealth -= damagePoints;
            }
		}
		Destroy (this.gameObject);
	}
}
